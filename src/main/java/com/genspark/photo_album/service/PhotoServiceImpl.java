package com.genspark.photo_album.service;

import com.genspark.photo_album.controller.PhotoRepository;
import com.genspark.photo_album.entity.Photo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PhotoServiceImpl implements PhotoService {

    @Autowired
    private PhotoRepository photoRepository;

    @Override
    public List<Photo> findAll() {
        return photoRepository.findAll();
    }

    @Override
    public Photo findByPhotoId(long id) {
        return photoRepository.findById(id);
    }

    @Override
    public Photo saveOrUpdatePhoto(Photo photo) {
        return photoRepository.save(photo);
    }

    @Override
    public void deletePhotoById(String id) {
        photoRepository.deleteById(id);
    }

}
