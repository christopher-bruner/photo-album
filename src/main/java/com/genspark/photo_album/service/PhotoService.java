package com.genspark.photo_album.service;

import com.genspark.photo_album.entity.Photo;

import java.util.List;

public interface PhotoService {

    List<Photo> findAll();
    Photo findByPhotoId(long id);
    Photo saveOrUpdatePhoto(Photo photo);
    void deletePhotoById(String id);

}
