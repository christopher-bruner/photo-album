package com.genspark.photo_album.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("photos")
public class Photo {

    @Id
    private String id;
    private User user;
    private String title;
    private String url;
    private int likes;
    private Comment[] comments;
    public Photo() {}

    public Photo(String id, User user, String title, String url, int likes, Comment[] comments) {
        this.id = id;
        this.user = user;
        this.title = title;
        this.url = url;
        this.likes = likes;
        this.comments = comments;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public Comment[] getComments() {
        return comments;
    }

    public void setComments(Comment[] comments) {
        this.comments = comments;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
