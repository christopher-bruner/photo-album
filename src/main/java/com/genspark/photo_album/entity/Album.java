package com.genspark.photo_album.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("albums")
public class Album {

    @Id
    private String id;
    private User owner;
    private String name;
    private Photo[] photos;

    public Album() {}

    public Album(String id, User owner, String name, Photo[] photos) {
        this.id = id;
        this.owner = owner;
        this.name = name;
        this.photos = photos;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Photo[] getPhotos() {
        return photos;
    }

    public void setPhotos(Photo[] photos) {
        this.photos = photos;
    }

}
