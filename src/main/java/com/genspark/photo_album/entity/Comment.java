package com.genspark.photo_album.entity;

import java.util.Date;

public class Comment {

    private User user;
    private String text;
    private Date submittedAt;

    public Comment() {
    }

    public Comment(User user, String text, Date submittedAt) {
        this.user = user;
        this.text = text;
        this.submittedAt = submittedAt;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getSubmittedAt() {
        return submittedAt;
    }

    public void setSubmittedAt(Date submittedAt) {
        this.submittedAt = submittedAt;
    }
}
