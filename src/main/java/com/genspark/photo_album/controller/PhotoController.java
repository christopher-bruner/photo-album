package com.genspark.photo_album.controller;

import com.genspark.photo_album.entity.Photo;
import com.genspark.photo_album.service.PhotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PhotoController {

    @Autowired
    private PhotoService photoService;

    @GetMapping("/image")
    public List<Photo> getAllPhotos() {
        return photoService.findAll();
    }

    @GetMapping("/image/{id}")
    public Photo getPhotoByPhotoId(@PathVariable("id") Long id) {
        return photoService.findByPhotoId(id);
    }

    @PostMapping("/image")
    public Photo saveOrUpdatePhoto(@RequestBody Photo photo) {
        return photoService.saveOrUpdatePhoto(photo);
    }

    @DeleteMapping("/image/{id}")
    public ResponseEntity<?> deletePhotoByPhotoId(@PathVariable("id") Long id) {
        photoService.deletePhotoById(id.toString());
        return new ResponseEntity<>("Image deleted", HttpStatus.OK);
    }

}
