package com.genspark.photo_album.controller;

import com.genspark.photo_album.entity.Album;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "album", path="albums")
public interface AlbumRepository extends MongoRepository<Album, String> {
}
