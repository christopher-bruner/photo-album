package com.genspark.photo_album.controller;

import com.genspark.photo_album.entity.Photo;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "photo", path = "photos")
public interface PhotoRepository extends MongoRepository<Photo, String> {

    Photo findById(long id);

    public long count();

}
